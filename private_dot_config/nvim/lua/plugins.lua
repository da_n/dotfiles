-- [[ plugins.lua ]]

require('packer').startup({
    function(use)
        use 'wbthomason/packer.nvim'

	    -- [[ UI ]]
        use {                                              -- filesystem navigation
            'kyazdani42/nvim-tree.lua',
            requires = 'kyazdani42/nvim-web-devicons'      -- filesystem icons
        }

        -- [[ Theme ]]
        use {
            'nvim-lualine/lualine.nvim',                   -- statusline
            requires = {'kyazdani42/nvim-web-devicons', opt = true}
        }
        use { 'Mofiqul/dracula.nvim' }                     -- colorscheme

        -- [[ LSP ]]
	    use 'williamboman/mason.nvim'                      -- manage lsp, debuggers, linters
	    use 'williamboman/mason-lspconfig.nvim'            -- extend mason
        use 'neovim/nvim-lspconfig'                        -- a collection of lsp configs 
        use 'simrat39/rust-tools.nvim'                     -- tools to automatically set up lspconfig for rust-analyzer
        use 'nvim-treesitter/nvim-treesitter'              -- parser generator tool

        -- [[ Dev ]]
        use {
            'nvim-telescope/telescope.nvim',               -- fuzzy finder
            requires = { {'nvim-lua/plenary.nvim'} }
        }
        use { 'majutsushi/tagbar' }                        -- code structure
        use { 'Yggdroot/indentLine' }                      -- see indentation
        use { 'tpope/vim-fugitive' }                       -- git integration
        use { 'junegunn/gv.vim' }                          -- commit history
        use { 'windwp/nvim-autopairs' }                    -- auto close brackets, etc.

        use 'hrsh7th/nvim-cmp'                             -- Completion framework 
        use 'hrsh7th/cmp-nvim-lsp'                         -- lsp completion sources
        use 'hrsh7th/cmp-nvim-lua'                         -- useful completion sources
        use 'hrsh7th/cmp-nvim-lsp-signature-help'          -- //
        -- use 'hrsh7th/cmp-vsnip'                            -- // 
        use 'hrsh7th/cmp-path'                             -- //
        use 'hrsh7th/cmp-buffer'                           -- //
        use 'hrsh7th/vim-vsnip'                            -- //

        use {
            'numToStr/Comment.nvim',
            config = function()
                require('Comment').setup()
            end
        }
    end,
    config = {
        package_root = vim.fn.stdpath('config') .. '/site/pack',
    }
})

